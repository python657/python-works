from abc import ABC,abstractmethod

class Student(ABC):
    @abstractmethod
    def Get(self):
        pass
    @abstractmethod
    def Put(self):
        pass

class Bsc(Student):
    def Get(self):
        self.__roll = input("Enter Roll  Number: ")
        self.__name = input("Enter Name : ")

        self.__p = int(input("Enter Marks Of Physics : "))
        self.__c = int(input("Enter Marks Of Chemistry : "))
        self.__m = int(input("Enter  Marks Of Maths : "))

    def Put(self):
        print("_______________________________________________________________________________________________________________________________________________")
        print('{:25s} {:32s} {:32s} {:20s} {:20s} {:20s}'.format("Roll Number","Name","Physics Mark", "Chemistry Mark", "Maths Mark", "Percentage" ))
        print("_______________________________________________________________________________________________________________________________________________")
        print('{:5d} {:>25} {:32d} {:32d} {:20d} {:24f}'.format(int(self.__roll),self.__name,int(self.__p),int(self.__c),int(self.__m),float((self.__p + self.__c + self.__m)/3)) )
        print("_______________________________________________________________________________________________________________________________________________")

class Ba(Student):
    def Get(self):
        self.__roll = input("Enter Roll  Number: ")
        self.__name = input("Enter Name : ")

        self.__g = int(input("Enter Marks Of Geology : "))
        self.__e = int(input("Enter  Marks Of Economics : "))
        
    def Put(self):
        print("_______________________________________________________________________________________________________________________________________________")
        print('{:25s} {:32s} {:32s} {:20s} {:20s}'.format("Roll Number","Name","Geology Mark", "Economics Mark", "Percentage" ))
        print("_______________________________________________________________________________________________________________________________________________")
        print('{:5d} {:>25} {:32d} {:32d}  {:24f}'.format(int(self.__roll),self.__name,int(self.__g),int(self.__e),float((self.__g + self.__e)/2)) )
        print("_______________________________________________________________________________________________________________________________________________")


bsc_student=Bsc()
bsc_student.Get()
ba_student=Ba()
ba_student.Get()
print("BSC STUDENT RESULT")
bsc_student.Put()
print("BA STUDENT RESULT")
ba_student.Put()
