# Example python program to find out the employee salary using single level inheritance
class Employee:
    def __init__(self, employee_name, employee_basic_salary):
        # initialize protected attributes
        self._employee_name = employee_name
        self._employee_basic_salary = employee_basic_salary
         
    # define protected method
    def _show(self):
        # access protected attributes inside class 
        print("Employee Name: ", )
        print("Employee Basic Salary: ", )
        employee_da=float(self._employee_basic_salary*0.25)
        employee_hra=float(self._employee_basic_salary*0.15)
        employee_pf=float((self._employee_basic_salary+employee_da)*0.12)
        employee_ta=float(self._employee_basic_salary*0.075)
        employee_netpay=float(self._employee_basic_salary+employee_da+employee_hra+employee_ta)
        employee_grosspay=float(employee_netpay-employee_pf)
        print("\n")
        print("S A L A R Y  D E T A I L E D  B R E A K U P")
        print("==============================================")
        print(" NAME OF EMPLOYEE : ",self._employee_name.upper())
        print(" BASIC SALARY : ",self._employee_basic_salary)
        print(" DEARNESS ALLOWANCE. : ",employee_da)
        print(" HOUSE RENT ALLOWANCE.: ",employee_hra)
        print(" TRAVEL ALLOWANCE. : ",employee_ta)
        print("==============================================")
        print(" NET SALARY PAY : ",employee_netpay)
        print(" PROVIDENT FUND : ",employee_pf)
        print("==============================================")
        print(" GROSS PAYMENT : ",employee_grosspay)
        print("==============================================")
         
class Salary(Employee):
    def __init__(self, employee_name, employee_basic_salary):
        Employee.__init__(self, employee_name, employee_basic_salary)
        self.employee_name = employee_name
    def salary_calculator(self):
        print("Employee Name: ", self.employee_name)
        # access Employee's protected method
        self._show()
       

try:
    employee_name = input("Enter Employee Name:")
    employee_basic_salary = float(input("Enter Employee Basic Salary:"))
    employee_object = Salary(employee_name,employee_basic_salary)
    employee_object.salary_calculator()

except Exception as error:
    print(f"Error occured inside the program: {error}")
