# python program for simple calculator 
class simple_calculator:
    def __init__(self, number_1, number_2):
        self.number_1 = number_1
        self.number_2 = number_2
    # define 'add' method
    def add(self):
        return self.number_1 + self.number_2
    # define 'subtract' method
    def subtract(self):
        return self.number_1 - self.number_2
    # define 'multiply' method 
    def multiply(self):
        return self.number_1 * self.number_2
    # define 'division' method 
    def division(self):
        return self.number_1 / self.number_2
if __name__ == '__main__':
    try:
        number_1 = float(input("Enter the first number:"))
        number_2 = float(input("Enter the second number:"))
        # create an instance 
        simple_calculator_object = simple_calculator(number_1,number_2)
    
        # call add method
        print(f'{number_1} + {number_2} = {simple_calculator_object.add()}')

    
        # call subtract method
        print(f'{number_1} - {number_2} = {simple_calculator_object.subtract()}')

        # call multiply method
        print(f'{number_1} * {number_2} = {simple_calculator_object.multiply()}')

        # call division method
        print(f'{number_1} / {number_2} = {simple_calculator_object.division()}')

    except Exception as error:
        print(f"Error occured inside the program: {error}")