import os
import datetime

try:
    today_date = datetime.datetime.now()
    file_age = int(input("Enter the file aging days:"))
    directory_path = str(input("Enter the directory path for searching:"))
    print(f"Files older than {file_age} inside {directory_path} directory")
    for dir,dirpath,filename in os.walk(directory_path):
        for file in filename:
            complete_path=os.path.join(dir,file)
            file_creation_time=datetime.datetime.fromtimestamp(os.path.getctime(complete_path))
            time_diff=(today_date-file_creation_time).days
            if time_diff> file_age:
                print(complete_path, time_diff)
except Exception as error:
    print(f"Error occured inside the program: {error}")