# Program to check the file or directory exists or not
import os

try:
    file_path = str(input("Enter the file or directory path need to check for the existence:"))
    file_path_value = os.path.exists(file_path)
    if file_path_value:
       print(f"{file_path} exists inside this system")
    else:
        print(f"File doesnot {file_path} exists")
except Exception as error:
    print(f"Error occured inside the program: {error}")