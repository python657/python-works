

## Python codes related to os modules


## Examples



- To print the current working directory, use os.getcwd(). This is similar to pwd command in Linux.

  ```
  Python 3.9.12 (main, Apr 18 2022, 22:40:46) 
  [GCC 9.4.0] on linux
  Type "help", "copyright", "credits" or "license" for more information.
  >>> import os
  >>> os.getcwd()
  '/home/osboxes/python-works'
  >>> 

  ```

- To change path/current working directory os.chdir(<directory to change>). This is similar to the cd command in Linux.

  ```
  # os.chdir(<directory to change>)
  >>> os.chdir("/tmp")# To verify your path now
  >>> os.getcwd()
  '/tmp'
  ```
- To print/list files in the current directory(it return a list) use os.listdir() . It’s similar to the ls -l command in Linux.
  ```
   osboxes   main  ~  python-works  ls -ltr
    total 36
    -rw-rw-r-- 1 osboxes osboxes 1007 Sep 22 12:39 thread-example.py
    -rw-rw-r-- 1 osboxes osboxes 2120 Sep 22 12:39 student-marklist-calculation.py
    -rw-rw-r-- 1 osboxes osboxes   18 Sep 22 12:39 README.md
    -rw-rw-r-- 1 osboxes osboxes  594 Sep 22 12:39 list-sentence.py
    -rw-rw-r-- 1 osboxes osboxes 1022 Sep 22 12:39 dict-key-check.py
    drwxrwxr-x 3 osboxes osboxes 4096 Sep 22 13:21 s3-policy
    drwxrwxr-x 2 osboxes osboxes 4096 Sep 23 10:58 python-subprocess
    -rw-rw-r-- 1 osboxes osboxes 1251 Sep 23 11:05 python-subprocess.py
    drwxrwxr-x 2 osboxes osboxes 4096 Sep 23 11:29 python-os

  ```
  ```
  >>> os.listdir()
  ['.git', 's3-policy', 'python-subprocess.py', 'dict-key-check.py', 'python-os', 'thread-example.py', 'student-marklist-calculation.py', 'list-sentence.py', 'python-subprocess', 'README.md']
  >>> 

  ```
- To create a directory os.mkdir().

  ```
  # To create a directoryos.mkdir(<directory name>)>>> os.mkdir("mytest")# To verify it
  >>> os.listdir()
  ['.git', 'mytest', 's3-policy', 'python-subprocess.py', 'dict-key-check.py', 'python-os', 'thread-example.py', 'student-marklist-calculation.py', 'list-sentence.py', 'python-subprocess', 'README.md']

  ```
- To print, all the environment variables use os.environ. This is similar to the env command in Linux.
  ```
  >>> os.environ
  environ({'SHELL': '/bin/bash', 'SESSION_MANAGER': 'local/osboxes:@/tmp/.ICE-unix/4063,unix/osboxes:/tmp/.ICE-unix/4063', 'WINDOWID': '52428803', 'QT_ACCESSIBILITY': '1', 'NVM_RC_VERSION': '', 'COLORTERM': 'truecolor', 'XDG_CONFIG_DIRS': '/etc/xdg/xdg-xfce:/etc/xdg:/etc/xdg', 'XDG_SESSION_PATH': '/org/freedesktop/DisplayManager/Session0', 'XDG_MENU_PREFIX': 'xfce-', 'LANGUAGE': 'en_US', 'MANDATORY_PATH': '/usr/share/gconf/xfce.mandatory.path', 'SSH_AUTH_SOCK': '/tmp/ssh-7cioll2LWCqR/agent.3921', 'SDKMAN_CANDIDATES_DIR': '/home/osboxes/.sdkman/candidates', 'DESKTOP_SESSION': 'xfce', 'SSH_AGENT_PID': '4045', 'GOBIN': '/home/osboxes/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/usr/local/go/bin:/usr/local/go/bin:/home/osboxes/.pulumi/bin:/usr/local/go/bin:/usr/local/go//bin', 'GTK_MODULES': ':canberra-gtk-module', 'XDG_SEAT': 'seat0', 'PWD': '/home/osboxes/python-works', 'LOGNAME': 'osboxes', 'XDG_SESSION_DESKTOP': 'xfce', 'QT_QPA_PLATFORMTHEME': 'qt5ct', 'XDG_SESSION_TYPE': 'x11', 'PANEL_GDK_CORE_DEVICE_EVENTS': '0', 'XAUTHORITY': '/home/osboxes/.Xauthority', 'XDG_GREETER_DATA_DIR': '/var/lib/lightdm-data/osboxes', 'GDM_LANG': 'en_US', 'HOME': '/home/osboxes', 'LANG': 'en_US.UTF-8', 'XDG_CURRENT_DESKTOP': 'XFCE', 'VTE_VERSION': '6003', 'SDKMAN_VERSION': '5.11.5+713', 'XDG_SEAT_PATH': '/org/freedesktop/DisplayManager/Seat0', 'GROOVY_HOME': '/home/osboxes/.sdkman/candidates/groovy/current', 'NVM_DIR': '/home/osboxes/.nvm', 'XDG_SESSION_CLASS': 'user', 'TERM': 'xterm-256color', 'DEFAULTS_PATH': '/usr/share/gconf/xfce.default.path', 'USER': 'osboxes', 'SDKMAN_DIR': '/home/osboxes/.sdkman', 'DISPLAY': ':0.0', 'SHLVL': '2', 'NVM_CD_FLAGS': '', 'XDG_VTNR': '7', 'SDKMAN_CANDIDATES_API': 'https://api.sdkman.io/2', 'XDG_SESSION_ID': 'c2', 'XDG_RUNTIME_DIR': '/run/user/1000', 'XDG_DATA_DIRS': '/usr/share/xfce4:/usr/local/share:/usr/share:/var/lib/snapd/desktop:/usr/share', 'PATH': '/home/osboxes/.sdkman/candidates/groovy/current/bin:/home/osboxes/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/usr/local/go/bin:/usr/local/go/bin:/home/osboxes/.pulumi/bin:/usr/local/go/bin:/home/osboxes/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/usr/local/go/bin:/usr/local/go/bin:/home/osboxes/.pulumi/bin:/usr/local/go/bin:/usr/local/go//bin', 'GDMSESSION': 'xfce', 'DBUS_SESSION_BUS_ADDRESS': 'unix:path=/run/user/1000/bus', 'SDKMAN_PLATFORM': 'linuxx64', 'GOPATH': '/home/osboxes/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/usr/local/go/bin:/usr/local/go/bin:/home/osboxes/.pulumi/bin:/usr/local/go/bin:/usr/local/go/', 'OLDPWD': '/home/osboxes', '_': '/usr/bin/python3.9'})
  >>> 
- If we are looking for any specific environment variable, os.environ.get(<variable name>)
  ```
    >>> os.environ.get('LOGNAME')
      'osboxes'
    >>> 

  ```
- To get the userid use os.getuid() or group id use os.getgid(). It’s similar to the id command in Linux.
  ```
  >>> os.getgid()
  1000
  >>>
  ```
- Remove directory or remove directory recursively use os.rmdir() or remove directory recursively use os.removedirs().

  ```
    >>> os.rmdir("mytest")>>> os.removedirs("test3/test4")
  ```
- OS Path module is a submodule of os and provides useful functions on pathnames.

  To get the base name or the final component of a pathname, use os.path.basename().

  ```
    >>> os.path.basename("/home/centos")
    'centos'
  ```
- To get the directory component of a pathname os.path.dirname().
  ```
  >>> os.path.dirname("/home/centos")
      '/home'
  ```

- To join two paths together, use os.path.join().
  ```
  >>> a = os.path.join(“/tmp”, “testhome”)
  >>> print(a)
  /tmp/testhome
  ```
- To split the pathname path into a pair, (head, tail) where the tail is the last pathname component and the head is everything leading up to that os.path.split().

  ```
    >>> os.path.split("/etc/tuned/recommend.d")
    ('/etc/tuned', 'recommend.d')
  ```

-  To return the size, in bytes, of the path os.path.getsize().

    ```
      >>> os.path.getsize("/etc/hosts")
      235

    ```
- To check if the path exists use os.path.exists(). It will return True if the path exists and False if it’s not.

  ```
  >>> os.path.exists("/etc/hosts")
  True
  >>> os.path.exists("/etc/hostsss")
  False
  ```

- In the same way, you can check the presence of file/directory os.path.isfile(). It will return True if the path exists and False if it’s not.

  ```
  >>> os.path.isfile("/etc/hosts")
  True
  >>> os.path.isfile("/etc")
  False
  >>> os.path.isdir("/etc")
  True
  ```

- To check if the given path is a symbolic link, use os.path.islink(). It will return True if the path exists and False if it’s not.

  ```
    >>> os.path.islink("/etc/rc0.d")
    True

### OS.system

os.system is used to execute operating system commands. If we want to list a directory, we can use the (os.listdir() command), but we don’t want to memorize functionality provided by the os module and want to use the existing Linux command, we can use the os.system(). The other advantage is if we look at the command output, it returns exit code also(if the command executed successfully is zero(0) or non-zero if the command doesn’t execute successfully.

  ```
  >>> os.system("ls")
  Desktop  Documents
  0
  ```
We can’t store the command’s output executed by the os.system(). The only thing we can store is the return code. However, as we can see in the below code, it only stores the return code and the ls command output. If we want to store the output of the ls command, we need to use the subprocess module

  ```
  >>> rt=os.system("ls")
  Desktop  Documents
  >>> print(rt)
  0
  ```
os.system (which is just a thin wrapper around the POSIX system call) runs the command in a shell launched as a child of the current process

    OS.walk

To print the directory tree and all the files within that directory tree, we can use os. walk, which is a generator and has a tuple of 3 values

    directories(dirpath)(The path you have given)
    directories within that path(dirname)
    files within that path
  ```
  >>> import os
  >>> os.walk("/etc/tuned")
  <generator object walk at 0x7f786ea508e0>>>> list(os.walk("/etc/tuned"))
  [('/etc/tuned', ['recommend.d'], ['active_profile', 'tuned-main.conf', 'bootcmdline', 'profile_mode']), ('/etc/tuned/recommend.d', [], [])] 
  ```

We can execute the below command.

  ```
  >>> for dirname, dirpath, filename in os.walk("/etc/tuned"):
  ...     print(dirname)
  ...     print(dirpath)
  ...     print(filename)
  ... 
  /etc/tuned
  ['recommend.d']
  ['active_profile', 'tuned-main.conf', 'bootcmdline', 'profile_mode']
  /etc/tuned/recommend.d
  ```

- Python Script to check if the file or directory exists

  ```
    # Program to check the file or directory exists or not
    import os

    try:
        file_path = str(input("Enter the file or directory path need to check for the existence:"))
        file_path_value = os.path.exists(file_path)
        if file_path_value:
          print(f"{file_path} exists inside this system")
        else:
            print(f"File doesnot {file_path} exists")
    except Exception as error:
        print(f"Error occured inside the program: {error}")
  ```
   - Output

      ![](path-1.png)


- A Python script to search files greater than X days old
  
  ```
    import os
    import datetime

    try:
        today_date = datetime.datetime.now()
        file_age = int(input("Enter the file aging days:"))
        directory_path = str(input("Enter the directory path for searching:"))
        print(f"Files older than {file_age} inside {directory_path} directory")
        for dir,dirpath,filename in os.walk(directory_path):
            for file in filename:
                complete_path=os.path.join(dir,file)
                file_creation_time=datetime.datetime.fromtimestamp(os.path.getctime(complete_path))
                time_diff=(today_date-file_creation_time).days
                if time_diff> file_age:
                    print(complete_path, time_diff)
    except Exception as error:
        print(f"Error occured inside the program: {error}")
  ```

  - Output

    ```
      osboxes   main  ~  python-works  python-os  python3.9 file-aging.py 
      Enter the file aging days:15
      Enter the directory path for searching:/var/log
      Files older than 15 inside /var/log directory
      /var/log/messages 772
      /var/log/mail.info 772
      /var/log/vboxadd-setup.log.3 373
      /var/log/alternatives.log.3.gz 113
      /var/log/alternatives.log.6.gz 113
      /var/log/dpkg.log.9.gz 16
      /var/log/ubuntu-advantage.log.1 16
      /var/log/mail.err 772
      /var/log/alternatives.log.4.gz 113
      /var/log/llupdates.log 137
      /var/log/ubuntu-advantage.log.2.gz 16
      /var/log/alternatives.log.5.gz 113
      /var/log/btmp.1 16
      /var/log/dpkg.log.5.gz 16
      /var/log/vboxadd-setup.log.4 373
      /var/log/vboxadd-setup.log 373
      /var/log/vboxadd-setup.log.2 373
      /var/log/ubuntu-advantage.log.4.gz 16
      /var/log/user.log 772
      /var/log/btmp 16
      /var/log/boot 772
      /var/log/mail.log 772
      /var/log/daemon.log 772
      /var/log/dpkg.log.1 16
      /var/log/ubuntu-advantage.log.5.gz 16
      /var/log/dpkg.log.8.gz 16
      /var/log/dpkg.log.10.gz 16
      /var/log/dpkg.log.7.gz 16
      /var/log/lastlog 476
      /var/log/dpkg.log.6.gz 16
      /var/log/lpr.log 772
      /var/log/vboxadd-install.log 610
      /var/log/alternatives.log.7.gz 113
      /var/log/vboxadd-uninstall.log 610
      /var/log/mail.warn 772
      /var/log/dpkg.log.3.gz 16
      /var/log/ubuntu-advantage.log.3.gz 16
      /var/log/faillog 476
      /var/log/alternatives.log.2.gz 113
      /var/log/dpkg.log.2.gz 16
      /var/log/dpkg.log.4.gz 16
      /var/log/alternatives.log.1 113
      /var/log/bootstrap.log 772
      /var/log/debug 772
      /var/log/vboxadd-setup.log.1 373
      /var/log/ubuntu-advantage.log.6.gz 16
      /var/log/dpkg.log.11.gz 16
      /var/log/cups/error_log.4.gz 371
      /var/log/cups/error_log.3.gz 371
      /var/log/cups/error_log.2.gz 371
      /var/log/cups/error_log.6.gz 371
      /var/log/cups/error_log 371
      /var/log/cups/error_log.1 371
      /var/log/cups/error_log.5.gz 371
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c7eb47fffdc9-9d2d048af7a3ce4c.journal~ 425
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005b9b6db3e583d-f7a428c0131fe20f.journal~ 605
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c94212b977d9-2cf11ed1208e8156.journal~ 408
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005b4c3b55e80b2-c18749103b15ff50.journal~ 668
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@980b415e31e64648877a3a7e861753e8-0000000000001306-0005ca481f42841a.journal 373
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@f2127f48bd764307851b46808a9516e7-000000000000061f-0005e0379b0dc5a2.journal 85
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c3d83a6585fa-ae85a866fea0831d.journal~ 476
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005b9bb749d6e6f-0c4ab0983d2b7593.journal~ 605
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@58b77b8e9c0d40edb49838e2d48f0fe5-0000000000000001-0005cbf8159a2f00.journal 137
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005b4c6fea6c5c8-df6155a8729323e2.journal~ 668
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@876c7f46cd63421b997c5d0979b2e2ec-00000000000004fa-0005b4c3bb0eb891.journal 668
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c814513f6e49-bd53608f304a7b26.journal~ 423
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005bd2c6156f6b2-95c5e51042f3c7ca.journal~ 561
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@4b89387c342c42dcae9458d4b49e83f5-0000000000000532-0005b62c2121504f.journal 610
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005e03753d45665-5e68efc7f892e29e.journal~ 115
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005bbf97f7e704f-b7a2265a3835aa85.journal~ 577
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@06e1d675e7b5482ab1847b0136c60f19-0000000000000001-0005ca47448c59d1.journal 395
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c9cee1cbe4ea-3db0bb772ab778ce.journal~ 401
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005bd2cfe61f80b-5004405d30ff01bf.journal~ 561
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@22dfea45b41e4e3b8dc6c97cbc490409-0000000000000001-0005b58d0ee2385b.journal 657
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@c021c3b665734262b402dc3a5aebd38c-000000000000057f-0005c3efdd9075af.journal 434
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005ca47449792df-0a6058221aac5912.journal~ 395
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@0005c3efdd90b915-3fe0009bcd10642f.journal~ 475
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c379fb379dcd-2622bad5b5313db4.journal~ 481
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@0005b9bb7906cfac-dd0591f4be3f0441.journal~ 605
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@2158c6f797f9457e9a4c202bc2fd017f-0000000000000001-0005aca2a75535cf.journal 772
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@0005c9cf03be74cf-cf0d205f75fa7b89.journal~ 401
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005cbf4a95c55f3-52ae90a03f6fe4da.journal~ 373
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@a3b44f44626e4e49bbe8f7c403cc5040-0000000000000715-0005aca294981388.journal 772
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@8c5b08ec708e4cba9b5822a58ea14a24-0000000000000001-0005bbf9881e1d29.journal 567
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c7c36d071e57-0f0c6bf7ee1fde34.journal~ 427
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005aca2a757bb61-aaf954259810f4cf.journal~ 772
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@0005c7c37288cf42-fd775933a71b635e.journal~ 427
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005b62c1cd31fd9-fdf637858d0490e7.journal~ 650
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@c45678ae6bac4b7f9eaebf3968338212-0000000000001964-0005bcb20be8c946.journal 564
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@a3b44f44626e4e49bbe8f7c403cc5040-0000000000000518-0005aca28549abe6.journal 772
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@0005bd2c66553b18-6acb59c0c04f18d9.journal~ 561
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005bcff200e9b49-617da4d0777deede.journal~ 564
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005b6290bcdc539-f59dbdab9419df02.journal~ 651
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@8c5b08ec708e4cba9b5822a58ea14a24-0000000000001965-0005bcb20bf117f8.journal 564
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@911dd51f45174949a88034da6fa05bb5-0000000000000001-0005cbf13079ccae.journal 373
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005b9bb85fd616f-c75e1a8c40413851.journal~ 605
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@ff5e288f37b04c62af0a093eba0b2c7b-00000000000019e4-0005b4c4a19d7df4.journal 668
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@0005b9bb8d8df996-4fcdac9c78d25682.journal~ 605
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@fc063b1737834dccb128e57cdfbea8d4-0000000000001d96-0005e2a4868c6498.journal 51
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@3e5cf1f9e76b419ab33b437079f7ee6f-0000000000000001-0005e03753caccb4.journal 85
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@73728aa6861e472b9700a91ae651b0f4-0000000000001970-0005b4c4a0e6bddd.journal 668
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@35b204d1912d4de896f91c96c5da0131-0000000000000001-0005b9db49ff52e5.journal 604
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005cbf815a4cc21-e2f7690c65640972.journal~ 373
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@be48e1147cff4670a876aa98fed75f14-00000000000020fd-0005b4c4ae6a7d06.journal 657
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@e76986c799fe4a9580fafb97ff043edc-0000000000000001-0005aca2820874ed.journal 772
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c7262fa650f7-4aea1ec8fd059946.journal~ 434
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c96c08fb6691-0836c7a985f47673.journal~ 405
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005bbf9882868b7-de1f257cd61e80dc.journal~ 577
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@a574636d1409442ea9c67cb7a414fa8f-0000000000000ca5-0005aca2c587e398.journal 668
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c7f3bd3e516c-429bccd50be4f45b.journal~ 424
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@6d528c91239d4f00af410a7a6ce8ef6c-0000000000000530-0005b9bb8d8d63a7.journal 604
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@c6759b3a51cb48dc9cfbf22daefa8a9b-0000000000000001-0005c7c36cf7549d.journal 426
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@ba72b884dc2b4bad931c64c0753a34be-0000000000000001-0005b950e53092ef.journal 610
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@c45678ae6bac4b7f9eaebf3968338212-000000000000054b-0005b9db522a28d3.journal 567
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@ae3e11f872e9445098a4dc3c636714a5-00000000000005c2-0005c7c37288946b.journal 426
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@0005e81031706bf6-5d38058d02d9ffb8.journal~ 16
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005e8102951ba72-35745f97f762e41a.journal~ 16
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@b6c2a3ee3f594692918620b12d96f2b8-0000000000000001-0005c7262f9ec934.journal 434
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c3d769a942a2-fadbeb5c28243644.journal~ 477
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@0005b4c3bb0ec98f-87e274630f3accea.journal~ 668
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@e76986c799fe4a9580fafb97ff043edc-00000000000006c2-0005aca28b351a81.journal 772
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@3e5cf1f9e76b419ab33b437079f7ee6f-0000000000001a11-0005e2a47e8bca7f.journal 51
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005b950e5373db2-1b9bbfdc0433cc3a.journal~ 610
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@2158c6f797f9457e9a4c202bc2fd017f-0000000000000514-0005aca2a9c219b5.journal 772
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005bcfedc677f09-c26f67f92f4488b4.journal~ 564
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@056630eb94ae45e3b1f10f3f56ebc832-0000000000000535-0005bd2d04526534.journal 481
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@416d185fbade43a0bcf73c90932dc570-00000000000005a7-0005cbf4af943310.journal 137
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@925a864fd8c54c56993f9a5eb996f62f-0000000000000540-0005b62c22946ff9.journal 610
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c80369333250-70a62f6e1c8ab07d.journal~ 423
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@73728aa6861e472b9700a91ae651b0f4-0000000000000001-0005b4c3b559ac88.journal 668
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@87e99ec6c06e4fd2b96097806d379aeb-000000000000613a-0005b60bb7bdd10c.journal 650
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005b95534a2ae31-78ec8b48d9a73e4e.journal~ 610
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005cbf130890e53-46dfb9c6c8c7a3e7.journal~ 373
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@2158c6f797f9457e9a4c202bc2fd017f-0000000000000c95-0005aca2c5722f97.journal 668
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005b4d5dc370982-183544d5fa014433.journal~ 667
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005b9db4a09d70c-30441f48b53f7bcd.journal~ 604
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@4fe8e6640a0541abbcdb972d559f549d-00000000000005fb-0005c9cf03be65f1.journal 395
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@387bf1c83af84f449b56c77afd45baf4-0000000000000531-0005aca2b08d5dd6.journal 772
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005b58d0eec67f2-96cdbd19049cf910.journal~ 658
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@0005e0379b0e007f-79e7a87264e097be.journal~ 115
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@4b89387c342c42dcae9458d4b49e83f5-0000000000000001-0005b62c1ccafe6b.journal 650
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c8f68b39c9ba-6d41c5dcd1aff0df.journal~ 411
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@27ce4c3529d94a24b8329b2fcc7fcabf-0000000000000001-0005c379fb2d76a5.journal 481
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@0005bd2d04527e4d-026a7b3b1e913984.journal~ 561
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/user-1000@0005aca2a9c34b5f-2cb213241ebe2ccc.journal~ 772
      /var/log/journal/55b959beaf3c4e8c885de71ed85401c1/system@0005c3efd0183c9c-879eba8051eb5a30.journal~ 475
      /var/log/installer/version 772
      /var/log/installer/telemetry 772
      /var/log/installer/initial-status.gz 772
      /var/log/installer/media-info 772
      /var/log/installer/casper.log 772
      /var/log/installer/syslog 772
      /var/log/installer/partman 772
      /var/log/installer/debug 772
      /var/log/apt/history.log.3.gz 16
      /var/log/apt/history.log.1.gz 16
      /var/log/apt/history.log.4.gz 16
      /var/log/apt/term.log.3.gz 16
      /var/log/apt/term.log.11.gz 16
      /var/log/apt/term.log.8.gz 16
      /var/log/apt/term.log.7.gz 16
      /var/log/apt/history.log.7.gz 16
      /var/log/apt/term.log.4.gz 16
      /var/log/apt/history.log.9.gz 16
      /var/log/apt/term.log.10.gz 16
      /var/log/apt/term.log.9.gz 16
      /var/log/apt/history.log.8.gz 16
      /var/log/apt/term.log.2.gz 16
      /var/log/apt/term.log.5.gz 16
      /var/log/apt/history.log.5.gz 16
      /var/log/apt/term.log.6.gz 16
      /var/log/apt/history.log.2.gz 16
      /var/log/apt/term.log.1.gz 16
      /var/log/apt/history.log.10.gz 16
      /var/log/apt/history.log.6.gz 16
      /var/log/apt/history.log.11.gz 16
    ```

    ![](file-age-output.png)