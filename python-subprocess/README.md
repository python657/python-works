

## Python program to kill a process id


## Outputs



- Program to kill the process named firefox

  ![](python-subprocess-2.png)

- Program to kill the process named firefox, but the process was not running

  ![](python-subprocess-1.png)
