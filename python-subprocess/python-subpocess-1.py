# python program to kill a particular process's id using os and subprocess modules

import os
import subprocess
from os import kill
from signal import SIGKILL
import json
from subprocess import Popen, PIPE
try:
   application_name = str(input("Enter the application name:"))
   application_name = application_name.lower()
   process = Popen(['ps', '-eo' ,'pid,args'], stdout=PIPE, stderr=PIPE)
   stdout, notused = process.communicate()
   processes_json_data = stdout.decode('utf8').replace("'", '"')
   processes_json_data = json.dumps(processes_json_data)
   processes_json_data = json.loads(processes_json_data)
   processes_id_list_for_kill = []
   for process_data in processes_json_data.splitlines():
         process_data = "/".join(process_data.split())
         process_list  = (process_data.split('/', 1))
         if any(application_name in s for s in process_list ):
            processes_id_list_for_kill.append(process_list[0])
   if processes_id_list_for_kill:
      print(f"Process ids related to the {application_name} applications are:\n")
      for pid in processes_id_list_for_kill:
            process_id = pid
            print(process_id)
            kill(int(process_id), SIGKILL)
      print(f"Successfully terminated all the process ids related to the {application_name} application ")
   else:
        print(f"No process ids found related to the {application_name} \n")

except Exception as error:
    print(f"No process found/error occured inside the program: {error}")