import json
import subprocess
import psutil
from os import kill
from signal import SIGKILL
from subprocess import Popen, PIPE

try:

    application_name = str(input("Enter the application name:"))
    application_name = application_name.lower()
    process = Popen(['ps', '-eo' ,'pid,args'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    grep_output = subprocess.Popen(['grep', '-i' , application_name], stdin=process.stdout,stdout=PIPE, stderr=PIPE)
    grep_output = grep_output.stdout.read().decode('utf-8')
    grep_output = json.dumps(list(grep_output.split("\n")))
    grep_output = json.loads(grep_output)
    processes_id_list_for_kill = []
    for process_data in grep_output:
        process_data = "/".join(process_data.split())
        process_list  = (process_data.split('/', 1))
        if process_list[0]:
            processes_id_list_for_kill.append(process_list[0])
    if processes_id_list_for_kill is not None:
        print(f"Process ids related to the {application_name} applications are:\n")
        for pid in processes_id_list_for_kill:
                process_id = int(pid)
                print(process_id)
                if psutil.pid_exists(process_id):
                    kill(int(process_id), SIGKILL)
        print(f"Successfully terminated all the process ids related to the {application_name} application ")
    else:
            print(f"No process ids found related to the {application_name} \n")
except Exception as error:
    print(f"{error}")
