# python program to kill a particular process's id using os and subprocess modules

import os
import subprocess
from os import kill
from signal import SIGKILL
import json
from subprocess import Popen, PIPE
try:
    application_name = str(input("Enter the application name:"))
    application_name = application_name.lower()
    pids_flag = 0
    ps_out = subprocess.Popen("ps -ef".split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read().decode('UTF-8').split("\n") # Launch command line and gather output
    for entry in ps_out:  # Loop over returned lines of ps
        if application_name in entry:
            process_id = entry.split()[1] # retrieve second entry in line
            print(process_id)
            kill(int(process_id), SIGKILL)
            pids_flag = 1
    if pids_flag == 1:
        print(f"Successfully terminated all the process ids related to the {application_name} application ")
    else:
        print(f"No process ids found related to the {application_name} \n")
except Exception as error:
    print(f"No process found/error occured inside the program: {error}")