# python program to kill a particular process's id using os and subprocess modules

import os
import subprocess
from os import kill
from signal import SIGKILL

try:
    
    application_name = str(input("Enter the application name:"))
    application_name = application_name.lower()
    process_listing_output = subprocess.run(args = [ "bash", "-c", "ps -aN --format cmd,pid | grep "  + application_name],
                                universal_newlines = True,
                                stdout = subprocess.PIPE)
    process_listing_output_lines = process_listing_output.stdout.splitlines()
    if process_listing_output_lines:
        print(f"Process ids related to the {application_name} applications are:\n")
        for pid in process_listing_output_lines:
            process_id = pid
            process_id = process_id.rsplit(' ', 1)[-1]
            print(process_id)
            kill(int(process_id), SIGKILL)
        print(f"Successfully terminated all the process ids related to the {application_name} application ")
    else:
        print(f"No process ids found related to the {application_name} \n")

except Exception as error:
    print(f"No process found/error occured inside the program: {error}")
